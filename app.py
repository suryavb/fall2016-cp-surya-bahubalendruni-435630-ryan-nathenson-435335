from flask import Flask, render_template, request, flash, redirect, url_for, session
from flask_pymongo import PyMongo
from datetime import datetime as dt
import os
from bs4 import BeautifulSoup
from bson.objectid import ObjectId
import urllib2
from wordcloud import WordCloud, STOPWORDS

working_dir = '/Users/Rishi/bitbucket/fall2016-cp-surya-bahubalendruni-435630-ryan-nathenson-435335/static/'

app = Flask(__name__)
app.secret_key = "BUSH DID 9/11"
app.static_folder = "static"
app.config['MONGO_URI'] = "mongodb://localhost:27017/word_cloud"
app.config['MONGO_DBNAME'] = "word_cloud"
client = PyMongo(app)


@app.route('/', methods=["GET", "POST"])
def index():
	if request.method == "GET":
		error = request.args.get('error', None)
		return render_template('index.html', error=error)

	elif request.method == "POST":
		username = request.form["username"]
		password = request.form["password"]
		submit_type = request.form["submit_type"]

		if submit_type == "Register":
			query = list(client.db.users.find({"username": username}))
			if len(query):
				error = "Username has been taken"
				return redirect(url_for('index', error=error))
			else:
				new_user = {"username": username, "password": password, "datetime": dt.now()}
				client.db.users.insert(new_user)
				session["username"] = username
				os.mkdir(working_dir + username)
				return redirect(url_for('view'))

		else:
			query = list(client.db.users.find({"username": username}))
			if len(query):
				if password != query[0]["password"]:
					error = "Password is incorrect"
					return redirect(url_for('index', error=error))
				else:
					session["username"] = username
					return redirect(url_for('view'))

			else:
				error = "Username does not exist"
				return redirect(url_for('index', error=error))


@app.route('/view', methods=["GET", "POST"])
def view():
	if request.method == "POST":
		if request.form["submit_type"] == "Logout":
			return redirect(url_for('logout'))

		else:
			return redirect(url_for('add'))

	if "username" not in session:
		return redirect(url_for('index'))

	else:
		documents = list(client.db.documents.find({"user": session.get("username")}))
		for doc in documents:
			doc["text"] = doc["text"][:1500] + "..."
			doc["image"] = url_for('static', filename=(session.get('username') + '/' + doc['image']))
		return render_template('view.html', documents=documents)


@app.route('/add', methods=["GET", "POST"])
def add():
	if request.method == "GET":
		return render_template('add.html')

	elif request.form["submit_type"] == "Logout":
		return redirect(url_for('logout'))

	elif request.form["submit_type"] == "View":
		return redirect(url_for('view'))

	else:
		title = request.form["title"]
		text = request.form["text"]
		wiki_search = request.form["wiki_search"]
		color = request.form["color"]
		image = title.replace(" ", "_") + ".png"
		if len(wiki_search) > 0 and len(text) > 0:
			error = "You entered both text and a wikipedia search"
			return render_template('add.html', error=error)
		if len(wiki_search) > 0 and len(text) == 0:
			wiki_search.replace(" ", "_")
			url = "https://en.wikipedia.org/wiki/" + wiki_search
			try:
				soup = BeautifulSoup(urllib2.urlopen(url), 'lxml')
				text = ""
				for p in soup.find('div', class_="mw-content-ltr").find_all('p'):
					text += p.get_text()
			except urllib2.HTTPError:
				error = "Your wikipedia search was invalid"
				return render_template('add.html', error=error)

		wc = WordCloud(background_color=color, max_words=200, stopwords=STOPWORDS, max_font_size=50, random_state=42)
		wc.generate(text)
		wc.to_file(working_dir + session.get("username") + "/" + image)
		wf_vec = {}
		for word in text.split(" "):
			if word in wf_vec:
				wf_vec[word] += 1
			else:
				wf_vec[word] = 1
		doc = {"title": title, "text": text, "color": color, "user": session["username"], "image": image, "wf_vec": wf_vec}
		client.db.documents.insert(doc)
		return redirect(url_for('view'))


@app.route('/logout')
def logout():
	session.pop('username', None)
	return redirect(url_for('index'))


@app.route('/delete')
def delete():
	if request.method == "GET":
		doc_id = request.args.get('doc_id', None)
		doc = list(client.db.documents.find({"_id": ObjectId(doc_id)}))[0]
		os.remove(working_dir + session.get("username") + "/" + doc["image"])
		client.db.documents.delete_one({"_id": ObjectId(doc_id)})
		return redirect(url_for('view'))


if __name__ == "__main__":
	app.run(debug=True, threaded=True)