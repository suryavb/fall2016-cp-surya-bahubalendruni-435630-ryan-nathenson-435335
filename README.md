# README #

* Create Rubric - 5
* Document data can be uploaded and stored in MongoDB - 20
* Data schema uses appropriate word frequency representations to be easily served by Flask - 10
* Word cloud visualization of documents - 20
* Users can save images if they choose - 10
* Users can specify color palettes - 10
* Site is visually appealing - 5

Creative Portion - 20
	 Ideas: 
	 use NLTK package in python to analyze sentiment of documents
	 use sentence length and structure to assess complexity of documents
	 User functionality to edit/delete documents